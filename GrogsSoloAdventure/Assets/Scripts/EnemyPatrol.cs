﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    //public variables
    public float forceStrength; //how fast we move
    public Vector2[] patrolPoints; //patrol points we will move to
    public float stopDistance; // how close we get before moving to next patrol point

    //private variables
    private Rigidbody2D ourRigidbody; //the rigidbody on this object used to move
    private int currentPoint = 0; //index of the current point we are moving towards

    // Awake is called when the script is loaded
    void Awake()
    {
        ourRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //How far are we from our target?
        float distance = (patrolPoints[currentPoint] - (Vector2)transform.position).magnitude;

        //IF we are closer to our target than our minimum distance....
        if (distance <= stopDistance)
        {
            //... Then Update to next target
            currentPoint = currentPoint + 1;

            // if we have gone past the end of our list
            // if our current point index is = or > length of our list
            if (currentPoint >= patrolPoints.Length)
            {
                //...THEN loop back to the start by setting current index to 0
                currentPoint = 0;
            }

        }

        //Move in the direction of our target

        //Get the direction we should move in
        //Subtracting target position from current position
        Vector2 direction = (patrolPoints[currentPoint] - (Vector2)transform.position).normalized;
        direction = direction.normalized;

        //Move in the correct direction with the set force strength
        ourRigidbody.AddForce(direction * forceStrength);
    }
}
