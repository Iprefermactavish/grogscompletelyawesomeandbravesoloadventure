﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChase : MonoBehaviour
{
    //Public Variables
    public float forceStrength; //how fast we move
    public Transform target; //the thing we want to chase

    //Private Variable
    private Rigidbody2D ourRigidbody; //this is the rigidbody attached to this object

    // Awake is called when the script first loads
    void Awake()
    {
        //Get the rigidbody that we'll be using for movement
        ourRigidbody = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        //Move in the direction of our target

        //Get the direction we should move in
        Vector2 direction = ((Vector2)target.position - (Vector2)transform.position).normalized;

        //Move in the correct direction with the set force strength
        ourRigidbody.AddForce(direction * forceStrength);
    }
}
