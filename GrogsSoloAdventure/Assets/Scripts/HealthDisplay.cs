﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour
{
    // This will contain the slider component attached to this object
    // Slider = variable is in the form of a Slider component
    Slider healthBar;
    PlayerHealth player;


    // Start is called before the first frame update
    void Start()
    {
        //Getting the Slider component off THIS game object
        // (the one this script is attached to)
        // and storing it in the healthBar variable
        healthBar = GetComponent<Slider>();

        // Search the entire scene for the PlayerHealth component
        // store it in the variable
        player = FindObjectOfType<PlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        // Create temporary float vairables
        // so we can use float division
        float currentHealth = player.GetHealth();
        float maxHealth = player.startingHealth;

        // The Slider value should be between 0 and 1
        // with 0 being empty and 1 being full
        // We divide te current health by the max health
        // to get a number bewteen 0 and 1
        healthBar.value = currentHealth / maxHealth;

    }
}
