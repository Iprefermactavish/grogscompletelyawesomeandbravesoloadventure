﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour
{
    //Unity Editor Public Variables
    public GameObject projectilePrefab;
    public Vector2 projectileDirection;
    public float projectileStrength;

    // ACTION: Fire a projectile

    public void FireProjectile()
    {
        //Clone Projectile
        //Declare variable to hold cloned object
        GameObject clonedProjectile;
        //Use Instantiate to clone the projectile and keep the result in our variable
        clonedProjectile = Instantiate(projectilePrefab);

        // Position Projectile on the Player
        clonedProjectile.transform.position = transform.position; 


        //Fire it in a direction
        //Declare a variable to hold the cloned object's rigidbody
        Rigidbody2D projectileRigidbody;
        //Get the rigidbody from our cloned projectile and store it in
        projectileRigidbody = clonedProjectile.GetComponent<Rigidbody2D>();
        // Set the Velocity on the rigidbody to the editor setting
        projectileRigidbody.velocity = projectileDirection * projectileStrength;

        //Play firing animation
        //Declare variable to hold aniamtion on our player
        Animator playerAnimator;
        //Get the animation already attached to our player to use it
        playerAnimator = GetComponent<Animator>();
        //Use the animator component to trigger an animation change for attack
        playerAnimator.SetTrigger("attack");

    }

}
