﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneToLoad : MonoBehaviour
{
    //Function must be called by us not automatically by unity
    public void LoadTargetScene(string sceneToLoad)
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}
