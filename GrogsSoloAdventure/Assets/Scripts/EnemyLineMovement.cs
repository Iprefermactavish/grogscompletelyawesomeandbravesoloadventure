﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLineMovement : MonoBehaviour
{
    // Public Variables 
    // exposed for editing in Unity Editor
    public float forceStrength; // how hard the script pushes the enemy aka how fast the enemy moves

    public Vector2 direction; // what direction the enemy should move in

    // Private variables
    // not editable in Editor and used to track data whilst game is running
    private Rigidbody2D ourRigidbody; //container for rigidbody & attached to this object

    // Awake is called when script is first loaded

    void Awake()
    {
        // Get and store rigidbody will be usung for movement
        ourRigidbody = GetComponent<Rigidbody2D>();

        //Normalise our direction
        //Normalise changes it to be length 2, so we can multiply it by speed / force later
        direction = direction.normalized;
    }

    // Update is called once per frame
    void Update()
    {
        //Move in the correct direction with the set force strength
        ourRigidbody.AddForce(direction * forceStrength);

    }
}
