﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeDestroy : MonoBehaviour
{
    //Public Variable
    public float goalTime; //number of seconds the object should live for (max time)
    //Private Variable
    private float startTime; //timestamp that our timer is starting at (timestamp = seonds since game started)

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time; //Time.time = current timestamp (time since the game started)
    }

    // Update is called once per frame
    void Update()
    {
        //Declare a variable to contain our end timestamp to be used in comparison
        float endTime;
        // The end timestamp is just the starttime timestamp + how long the object should live (max time)
        endTime = startTime + goalTime;

        //Check IF our current time is greater than equal to our end time
        // IF it is, perform our action
        if (Time.time >= endTime)
        {
            //ACTION: destory object
            Destroy(gameObject);
        }
    }
}
