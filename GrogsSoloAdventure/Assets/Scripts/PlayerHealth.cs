﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerHealth : MonoBehaviour
{
    // Starting health for the player
    // Public variable = shown in Unity editor and accessible from other scripts
    // int = whole numbers, no decimals
    public int startingHealth;
    public string gameOverScene;

    // This will be the player's current health
    // Private variable = NOT shown in Unity or accessible from other scripts
    // int = whole numbers
    int currentHealth;

    // Built in Unity function called when the object this script is attached to is created
    // Usually this is when the game starts unless the object is spawned in later
    // this happens BEFORE the Start() function
    // Usually used for initialisation
    void Awake()
    {
        // Initialise our current health to be equal to our starting health
        // At begining of game
        currentHealth = startingHealth;
    }

    // Not built into Unity
    // We must call it ourselves
    // This wull change the player's current health
    // And Kill() them if they drop to 0 or less health
    // Public so other scripts can access it
    public void ChangeHealth(int changeAmount)
    {
        // Take our current health, add the change amount, and store
        // the result back in the current health variable
        currentHealth = currentHealth + changeAmount;

        // Keep our current health between 0 and the starting health
        currentHealth = Mathf.Clamp(currentHealth, 0, startingHealth);

        // if our health drops to 0, that means the player should die
        if (currentHealth <= 0)
        {
            // We call the Kill()
            Kill();
        }
    }


    // Function is NOT built in to Unity
    // It will only be called manually by our own code
    // It must be marked PUBLIC so our other scripts can access it
    public void Kill()
    {
        // This will destory the gameObject that this script is attached to
        //Destroy(gameObject);

        // Reload the Scene
        SceneManager.LoadScene(gameOverScene);
    }

    // Getter function to give information to the calling code
    // int means that an integer will be given back
    // return is what will be given back
    public int GetHealth()
    {
        //return will give the following info back to the calling code
        return currentHealth;

    }
}
