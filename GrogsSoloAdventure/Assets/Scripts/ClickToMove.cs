﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ClickToMove : MonoBehaviour
{
    // ------------------------------------------------
    // Public variables, visible in Unity Inspector
    // Use these for settings for your script
    // that can be changed easily
    // ------------------------------------------------

    // This variable will hold the strength of the force
    // propelling us toward our target
    // (ie our speed)
    public float forceStrength;
    // This variable will determine how close we get to 
    // the target point before stopping
    public float stopDistance;

    // ------------------------------------------------
    // Private variables, NOT visible in the Inspector
    // Use these for tracking data while the game
    // is running
    // ------------------------------------------------

    // This variable will hold the last point we touched
    // so we can keep moving to that point even after we
    // stop touching the screen
    private Vector2 targetPoint;

    // This variable will store the attached RigidBody 
    // so we can use it to move
    Rigidbody2D ourRigidbody;


    // ------------------------------------------------
    // Awake is called when the script is loaded
    // ------------------------------------------------
    void Awake()
    {
        // Get the rigidbody that we'll be using for movement
        ourRigidbody = GetComponent<Rigidbody2D>();

        // Set our target point to be our current position
        // so we don't start moving right away
        targetPoint = transform.position;
    }

    // ------------------------------------------------
    // Update is called once per frame
    // ------------------------------------------------
    void Update()
    {
        // If the mouse button is down or touch is detected...
        if (Input.GetMouseButton(0))
        {
            // Record the mouse / touch position as our target
            // Do this by using the camera to convert from screen 
            // coordinates to game world coordinates
            targetPoint = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        // How far away are we from the target?
        float distance = (targetPoint - (Vector2)transform.position).magnitude;

        // If we are farther away from our target than our minimum distance...
        if (distance > stopDistance)
        {
            // ... Move in the direction of our target

            // Get the direction
            // Subtract the current position from the target position to get a distance vector
            // Normalise changes it to be length 1, so we can then multiply it by our speed / force
            Vector2 direction = (targetPoint - (Vector2)transform.position).normalized;
            GetComponent<PlayerFire>().projectileDirection = direction;

            // Move in the correct direction with the set force strength
            ourRigidbody.AddForce(direction * forceStrength);
        }

        // Find out from the rigidbody what our current horizontal and vertical speeds are
        float currentSpeedV = ourRigidbody.velocity.y;

        // Get the animator that we'll be using for movement
        Animator ourAnimator = GetComponent<Animator>();

        // Tell our animator what the speeds are
        ourAnimator.SetFloat("speedV", currentSpeedV);
    }
}
