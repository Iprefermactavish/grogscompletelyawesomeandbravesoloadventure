﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    public string myMessage;
    public string mySecondMessage;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(myMessage);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(mySecondMessage);
    }
}
