﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamage : MonoBehaviour
{
    //CONDITION: When the characyer hits an object (enemy)
    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        //Check if the object has the tag we are looking for (Enemy)
        if (otherCollider.CompareTag("Enemy") == true)
        {
            //perform our action
            KillEnemy(otherCollider.gameObject);
        }
    }


    //ACTION: Destory Oject(Enemy)
    public void KillEnemy(GameObject enemy)
    {
        Destroy(enemy);
    }
}
